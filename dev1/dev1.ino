#include "Adafruit_NeoPixel.h"

/////////////////////////////
// Hardware
/////////////////////////////

// Anzahl Pixel pro Node
const int num_pixel = 1;

// Anzahl Levels im Netzwerk
const int num_levels = 7;

// Gesamtanzahl der Nodes
const int num_nodes = 22;

// Nodes - Levels - Zuweisung
const byte node_in_level[num_levels] = {3, 4, 3, 5, 4, 2, 1};

// LED-Strip: Objekt, Anzahl Pixel & Pins
Adafruit_NeoPixel strip = Adafruit_NeoPixel(num_nodes * num_pixel, 11, NEO_GRB + NEO_KHZ800);


/////////////////////////////
// Konfiguration
/////////////////////////////

// Richtung invertieren
//---------------------

// Richtung invertieren?
const boolean inversion = true;


// Farbe: Einfarbig
//-----------------

const boolean set_solid_color = true;
const uint32_t solid_color = strip.Color(200, 200, 255);

// Farbe: Farbe pro Level
//-----------------------

const boolean set_level_color = false;
const uint32_t level_color[] = {
  strip.Color(200, 200, 255),
  strip.Color(200, 255, 200),
  strip.Color(255, 200, 200),
  strip.Color(255, 200, 255),
  strip.Color(200, 255, 255)
};

// Farbe: Zufall aus Liste
//-------------------------

const boolean set_random_color = false;
const int num_random_color = 5;
const uint32_t random_color[] = {
  strip.Color(200, 200, 255),
  strip.Color(200, 255, 200),
  strip.Color(255, 200, 200),
  strip.Color(255, 200, 255),
  strip.Color(200, 255, 255)
};


// Helligkeit
//-----------

// maximale Helligkeit eines einzigen Nodes, Ganzzahlen, 0-255
const int brightness = 255;

//Nachglühen der bisherigen Strecke, Ganzzahlen, 0-255
const int afterglow = 20;


//Geschwindigkeit/Übergänge
//-------------------------

// Zeitdauer, wie lange ein Node alleine leuchtet in ms, nur positive Ganzzahlen
const int onenode = 300;

// Flanken-Steilheit, je höher, desto langsamer, nur positive Ganzzahlen & 0
const int steepness = 2;

// Flanken-Verschiebung, je höher, desto später, nur positive Ganzzahlen & 0
const int aberration = 0;

// Geschwindigkeit Fadeout, je höher, desto langsamer, nur positive Ganzzahlen & 0
const int fadeout = 0;


/////////////////////////////
// Ende Konfiguration
/////////////////////////////


/*********************************
 * Global Scope: Zustandsvariabeln
 *********************************/

//Aktuelle Stufe im Netzwerk
int current_level = 0;

//letzer manipulierter Punkt
int last_node = -1;


/******************************
 * Setup & Main
 ******************************/

void setup() {
  strip.begin();
  strip.show();

}

void loop() {
  
  if(inversion == false) {
    for(current_level = 0, last_node = -1; current_level < num_levels; current_level++) {
      if(current_level != 0) delay(onenode);
      int next_node = selectRandomNode();
      switchNode(last_node, next_node);
      last_node = next_node;  
    }
  }
  else {
    for(current_level = num_levels - 1, last_node = -1; current_level >= 0; current_level--) {
      if(current_level != num_levels - 1) delay(onenode);
      int next_node = selectRandomNode();
      switchNode(last_node, next_node);
      last_node = next_node;  
    }
  }
  fadeOut();
}


/*********************************
 * Nodes/Layers steuern
 *********************************/

int getLevelOffset() {
  int level_offset = 0;
  for(int i = 0; i < current_level; i++) {
    level_offset += node_in_level[i];
  }
  return level_offset;
}

int selectRandomNode() {
  return random(getLevelOffset(), getLevelOffset() + node_in_level[current_level]);
}


/*********************************
 * Pixel Manipulationen
 *********************************/

void switchNode(int fromNode, int toNode) {
  int toBrightness, fromBrightness;
  for(int toCounter = aberration * -1, fromCounter = brightness; toCounter < brightness; toCounter++, fromCounter--) {  
    fromBrightness = constrain(map(fromCounter, brightness, 0, brightness, afterglow), afterglow, brightness);
    toBrightness = constrain(toCounter, 0, brightness);
    if(fromNode > -1) {
      writeBrightness(fromNode, fromBrightness);
    }
    writeBrightness(toNode, toBrightness);
    delay(steepness);    
  }
}

void writeBrightness(int toNode, int nodeBrightness) {
  for(int i = 0; i < num_pixel; i++) {
    if(set_solid_color) {
      strip.setPixelColor(toNode * num_pixel + i, modBrightness(solid_color, 255, nodeBrightness));
    }
    else if(set_level_color) {
      strip.setPixelColor(toNode * num_pixel + i, modBrightness(level_color[current_level], 255, nodeBrightness));
    }
    else if(set_random_color) {
      strip.setPixelColor(toNode * num_pixel + i, modBrightness(random_color[random(num_random_color)], 255, nodeBrightness));
    }
  strip.show(); 
  }
}

void fadeOut() {
  for(int i = 255; i >= 0; i--) {
    for(int j = 0; j < num_pixel * num_nodes; j++) {
        int to_modulator = constrain(i - 1, 0, 255);
        uint32_t color_to_mod = modBrightness(strip.getPixelColor(j), i, 255);
        strip.setPixelColor(j, modBrightness(color_to_mod, 255, to_modulator));
    }
    strip.show();
    delay(fadeout);
  }
}

/**********
 * Utilites
************/

uint32_t modBrightness(uint32_t color, byte from_modulator, byte to_modulator) {
  byte b = constrain(map(color & 0x0000FF, 0, from_modulator, 0, to_modulator), 0, 255);
  byte g = constrain(map((color & 0x00FF00)>>8, 0, from_modulator, 0, to_modulator), 0, 255);
  byte r = constrain(map((color & 0xFF0000)>>16, 0, from_modulator, 0, to_modulator), 0, 255);
  uint32_t c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}
